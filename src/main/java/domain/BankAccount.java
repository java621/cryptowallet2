package domain;

import Exceptions.InsufficientBalanceExeption;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

//Wir möchten diese Datei später in eine externe Datei speichern, daher wird das Interface Serializable implementiert.
public class BankAccount implements Serializable {

    private BigDecimal balance;

    public BankAccount() {
        this.balance = new BigDecimal("0").setScale(2, RoundingMode.HALF_UP);
        BigDecimal result = this.balance.add(new BigDecimal("2")).setScale(2,RoundingMode.HALF_UP);
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    /**
     * Einzahlen auf das Bankkonto.
     * @param amount mitgabe der Menge an Geld die Sie einzahlen.
     */
    public void deposit(BigDecimal amount){
        if(amount != null) {
            this.balance = this.balance.add(amount).setScale(2, RoundingMode.HALF_UP);
        }
    }

    /**
     * Abheben vom Bankkonto.
     * @param amount mitgabe der Menge an Geld die Sie abheben möchten.
     * @throws InsufficientBalanceExeption falls zuwenig Geld am Konto ist als Sie abheben möchten.
     */
    public void withdraw(BigDecimal amount) throws InsufficientBalanceExeption{
        if(amount != null){
            if(this.balance.subtract(amount).doubleValue()>=0) {
                this.balance = this.balance.subtract(amount).setScale(2, RoundingMode.HALF_UP);
            }else{
                throw new InsufficientBalanceExeption();
            }
        }
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}
