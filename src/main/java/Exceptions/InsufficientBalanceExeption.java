package Exceptions;

public class InsufficientBalanceExeption extends Exception {
    public InsufficientBalanceExeption(){
        super("Insufficent Account Balance");
    }
}
