package at.hakimst.sample;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ResourceBundle;

import Exceptions.*;
import domain.*;
import infrastruktur.CurrentCurrencyPrices;
import infrastruktur.FileDataStore;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        AnchorPane root = FXMLLoader.load(Main.class.getResource("main.fxml"),
                ResourceBundle.getBundle("at.hakimst.sample.main"));

        Scene scene = new Scene(root, 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) throws InvalidFeeException {
        BankAccount ba = new BankAccount();
        ba.deposit(new BigDecimal("1000"));

        CryptoCurrency cryptoCurrency = CryptoCurrency.BTC;
        System.out.println(cryptoCurrency.getCurrencyName());
        System.out.println(cryptoCurrency.getCode());

        Transaction transaction = new Transaction(CryptoCurrency.ETH,
                new BigDecimal("1.23"),
                new BigDecimal("1567.8"));

        System.out.println(transaction);



        Wallet wallet = new Wallet("My BTC Wallet", CryptoCurrency.BTC, new BigDecimal("1"));
        System.out.println(wallet);
        try {
            wallet.buy(new BigDecimal("10"), new BigDecimal("5"), ba);
        } catch (InvalidAmountException e) {
            e.printStackTrace();
        } catch (InsufficientBalanceExeption insufficientBalanceExeption) {
            insufficientBalanceExeption.printStackTrace();
        }
        System.out.println(ba);
        System.out.println(wallet);


        WalletList walletList = new WalletList();
        walletList.addWallet(wallet);
        System.out.println(walletList);


        DataStore dataStore = new FileDataStore();
        try {
            dataStore.saveBankAccount(ba);
        } catch (SaveDataException e) {
            e.printStackTrace();
        }
        try {
            dataStore.retrieveBankAccount();
        } catch (RetrieveDataException e) {
            e.printStackTrace();
        }

        try {
            dataStore.saveWalletList(walletList);
        } catch (SaveDataException e) {
            e.printStackTrace();
        }
        try {
            WalletList walletList1 = dataStore.retrieveWalletList();
            System.out.println(walletList1);
        } catch (RetrieveDataException e) {
            e.printStackTrace();
        }

        launch(args);
    }
}
